package com.libertymutual.david_desjardins.programs.example101.shapes;

import java.awt.Color;
import java.math.BigDecimal;

abstract public class Shape {

    private Color color;

    public Shape(Color color) {
        this.color = color;
    }

    public abstract BigDecimal getArea();

    public Color getColor() {
        return color;
    }
}
