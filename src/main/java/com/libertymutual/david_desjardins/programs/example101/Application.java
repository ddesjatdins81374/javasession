package com.libertymutual.david_desjardins.programs.example101;

import com.libertymutual.david_desjardins.programs.example101.shapes.Circle;
import com.libertymutual.david_desjardins.programs.example101.shapes.Square;
import com.libertymutual.david_desjardins.programs.example101.shapes.Shape;
import com.libertymutual.david_desjardins.programs.example101.shapes.Rectangle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.Color;
import java.math.BigDecimal;


public class Application {

    private final static Logger logger = LogManager.getLogger(Application.class);

	public static void main(String[] args) {

        logger.info(Application.class + " starting ");

        int radius = 10;
        Circle circle = new Circle(radius, Color.PINK);
        System.out.println(circle.getArea());

        int length = 100;
        Square square = new Square(length, Color.RED);
        System.out.println(square.getArea());
		
		int reclength = 100;
		int recwidth = 100;
        Rectangle rectangle = new Rectangle(reclength, recwidth, Color.GREEN);
        System.out.println(rectangle.getArea());

    }

}
