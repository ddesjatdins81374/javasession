package com.libertymutual.david_desjardins.programs.example101.shapes;

import java.awt.Color;
import java.math.BigDecimal;

public class Rectangle extends Shape {

    private int length;
	private int width;

    public Rectangle(int length,int width, Color color) {
        super(color);
        this.length = length;
	    this.width = width;
    }

    // provide a getArea implementation
    @Override
    public BigDecimal getArea() {
        double area = length * width;
        return new BigDecimal(area);
    }
}
