package com.libertymutual.david_desjardins.programs.example101.shapes;

import java.awt.Color;
import java.math.BigDecimal;

public class Circle extends Shape {

    private int radius;

    public Circle(int radius, Color color) {
        super(color);
        this.radius = radius;
    }

    // provide a getArea implementation
    @Override
    public BigDecimal getArea() {
        double pi = 3.14;
        double area = pi * radius * radius;
        return new BigDecimal(area); 
    }

}
