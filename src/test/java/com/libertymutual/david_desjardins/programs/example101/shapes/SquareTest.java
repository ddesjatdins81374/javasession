package com.libertymutual.david_desjardins.programs.example101.shapes;

import org.junit.Test;

import java.awt.Color;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class SquareTest {

    @Test
    public void testGetArea() {
        int area = 10;
        Square square = new Square(area, Color.red);
        int expectedArea = area * area;
        BigDecimal expectedAnwser = new BigDecimal(expectedArea);
        BigDecimal actualAnswer = square.getArea();

        assertEquals(  "Verify that the area is correct", expectedAnwser, actualAnswer);
    }

}
