package com.libertymutual.david_desjardins.programs.example101.shapes;

import org.junit.Test;
import static org.junit.Assert.fail;
import java.awt.Color;
import java.math.BigDecimal;
import static org.junit.Assert.assertEquals;

public class RectangleTest {

    @Test
    public void testGetArea() {
        Rectangle rectangle = new Rectangle(100, 100, Color.red);
        BigDecimal area = rectangle.getArea();
        BigDecimal expectedAnwser = new BigDecimal(10000);
        assertEquals("Verify that the area is correct", expectedAnwser, area);
    }

}
